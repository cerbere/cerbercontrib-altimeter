import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader
from dateutil import parser

DEFAULT_TIME_UNITS = "seconds since 2000-01-01 00:00:00.0"


class ESAEnvisatRA2(BaseReader):
    pattern: str = re.compile(
        r"^.*ENV_RA_2_MWS____[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}.*PAC_R_NT_003.nc$",
    )
    engine: str = "netcdf4"
    description: str = "Use ENVISAT version 3 SGDR products from ESA in Xarray"
    url: str = "https://link_to/your_backend/documentation"
    time_units: str = DEFAULT_TIME_UNITS

    @classmethod
    def open_dataset(cls, filename_or_obj: Union[str, Path], **kwargs):
        kwargs['decode_times'] = False
        return super().open_dataset(filename_or_obj, **kwargs)


class ESAEnvisatRA21Hz(ESAEnvisatRA2):

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        drop_vars = []
        for k, v in ds.variables.mapping.items():
            if 'time_20' in v.dims or 'time_2k' in v.dims:
                drop_vars.append(k)
        ds = ds.drop_vars(drop_vars)

        ds = (ds.rename_vars(
            {'time_01': 'time', 'lat_01': 'lat', 'lon_01': 'lon'}
        ).swap_dims({'time_01': 'time'}))

        ds = xr.decode_cf(
            ds, drop_variables=['time_2k', 'UTC_day_01', 'UTC_sec_01'])

        # longitude convention
        attrs = ds['lon'].attrs
        encoding = ds['lon'].encoding
        lon = ds.lon.values
        lon[lon > 180] -= 360
        ds['lon'] = xr.DataArray(data=lon, dims='time')
        ds['lon'].attrs = attrs
        ds['lon'].encoding = encoding

        ds.attrs['time_coverage_start'] = parser.parse(
            ds.attrs['ra2_first_meas_time'])
        ds.attrs['time_coverage_end'] = parser.parse(
            ds.attrs['ra2_last_meas_time'])

        return ds


class ESAEnvisatRA220Hz(ESAEnvisatRA2):

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        drop_vars = []
        for k, v in ds.variables.mapping.items():
            if 'time_01' in v.dims or 'time_2k' in v.dims:
                drop_vars.append(k)
        ds = ds.drop_vars(drop_vars)

        ds = (ds.rename_vars(
            {'time_20': 'time', 'lat_20': 'lat', 'lon_20': 'lon'}
        ).swap_dims({'time_20': 'time'}))

        # longitude convention
        attrs = ds['lon'].attrs
        encoding = ds['lon'].encoding
        lon = ds.lon.values
        lon[lon > 180] -= 360
        ds['lon'] = xr.DataArray(data=lon, dims='time')
        ds['lon'].attrs = attrs
        ds['lon'].encoding = encoding

        ds['lon'].attrs['axis'] = 'X'
        ds['lat'].attrs['axis'] = 'Y'

        ds = xr.decode_cf(
            ds, drop_variables=['time_2k', 'UTC_day_01', 'UTC_sec_01'])

        ds.attrs['time_coverage_start'] = parser.parse(
            ds.attrs['ra2_first_meas_time'])
        ds.attrs['time_coverage_end'] = parser.parse(
            ds.attrs['ra2_last_meas_time'])

        return ds
