import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader


class AVISOJason(BaseReader):
    pattern: str = re.compile(
        r"^.*JA[1-2]_GPS_.*_[0-9]{8}_[0-9]{6}_[0-9]{8}.*.nc$")
    engine: str = "netcdf4"
    description: str = ("For reading AVISO Jason 1 & 2 IGDR, GDR and "
                        "SGDR products in version D and E with cerbere."
                        "Also works with NSOAS HY2A SDR.")
    url: str = "https://gitlab.ifremer.fr/cerbere/cerbercontrib-altimeter/"


class AVISOJason1Hz(AVISOJason):

    @classmethod
    def postprocess(cls, dst: xr.Dataset, **kwargs):
        # remove 20 Hz related variables
        dst = dst.drop_dims("meas_ind")

        # HY-2A
        if 'longitude' in dst:
            dst = dst.rename({'longitude': 'lon', 'latitude': 'lat'})

        # longitudes between -180 and 180
        dst['lon'][dst['lon'] > 180] -= 360

        return dst


class AVISOJason20Hz(AVISOJason):

    @classmethod
    def postprocess(cls, dst: xr.Dataset, **kwargs):

        # keep only variables with meas_ind dimension
        vars_20hz = []
        for v in dst.variables:
            dims_20hz = set(['time', 'meas_ind'])
            if len(set(dst[v].dims).intersection(dims_20hz)) in [0, 2]:
                vars_20hz.append(v)
        dst = dst[vars_20hz]

        # HY-2A
        if 'longitude' in dst:
            dst = dst.rename({'longitude': 'lon', 'latitude': 'lat'})

        # flatten
        dst = dst.stack(z=('time', 'meas_ind')).reset_index('z').drop_vars(
            ['time', 'lat', 'lon']).rename(
            {'time_20hz': 'time', 'lat_20hz': 'lat', 'lon_20hz': 'lon'}
        ).swap_dims({'z': 'time'}).reset_coords(['meas_ind'])

        # longitudes between -180 and 180
        dst['lon'][dst['lon'] > 180] -= 360

        return dst
