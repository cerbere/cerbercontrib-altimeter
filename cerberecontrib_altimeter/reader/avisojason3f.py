import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader


class AVISOJason3F(BaseReader):
    pattern: str = re.compile(r"^.*JA3_GPS_.*_[0-9]{8}_[0-9]{6}_[0-9]{8}.*.nc$")
    engine: str = "netcdf4"
    description: str = "Use AVISO Jason-3 Version F IGDR, GDR and SGDR products in Xarray"
    url: str = "https://link_to/your_backend/documentation"
    group_name: str = '/data_01'

    @classmethod
    def open_dataset(cls, filename_or_obj: Union[str, Path], **kwargs):
        dst = xr.open_dataset(filename_or_obj=filename_or_obj, group=cls.group_name, **kwargs)

        ds_c = xr.open_dataset(
            filename_or_obj=filename_or_obj,
            group='{0}{1}'.format(cls.group_name, '/c'),
            **kwargs,
        )
        ds_c = ds_c.rename_vars({var: '{0}{1}'.format('c_', var) for var in ds_c.variables})

        ds_ku = xr.open_dataset(
            filename_or_obj=filename_or_obj,
            group='{0}{1}'.format(cls.group_name, '/ku'),
            **kwargs,
        )
        ds_ku = ds_ku.rename_vars({var: '{0}{1}'.format('ku_', var) for var in ds_ku.variables})

        ds = xr.open_dataset(filename_or_obj=filename_or_obj)

        merged_ds = xr.merge([dst, ds_c, ds_ku])
        merged_ds.attrs = ds.attrs

        ds.close()

        merged_ds = merged_ds.drop_vars(['time_tai'])

        # change lon to [-180,180]
        merged_ds['longitude'][merged_ds.longitude > 180] -= 360

        return merged_ds


class AVISOJason3F1Hz(AVISOJason3F):
    group_name: str = '/data_01'


class AVISOJason3F20Hz(AVISOJason3F):
    group_name: str = '/data_20'
