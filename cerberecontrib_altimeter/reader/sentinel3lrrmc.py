import re

import xarray as xr
from cerbere.reader.basereader import BaseReader


class Sentinel3LRRMC(BaseReader):
    pattern: str = re.compile(r"^.*S3A_LRRMC_SGDR_.*_[0-9]{8}_[0-9]{6}_[0-9]{8}.*_V2-1_PEACHI_PRODUCT.nc$")
    engine: str = "netcdf4"
    description: str = "Use CNES/CLS Sentinel-3A products retracked with LR-RMC retracker in Xarray"
    url: str = "https://link_to/your_backend/documentation"

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        # longitude convention
        attrs = ds['lon'].attrs
        encoding = ds['lon'].encoding
        if 'lon' in ds.indexes:
            ds = ds.reset_index('lon')
        lon = ds.lon.values
        lon[lon > 180] -= 360
        ds['lon'] = xr.DataArray(data=lon, dims='time')
        ds['lon'].attrs = attrs
        ds['lon'].encoding = encoding
        return ds
