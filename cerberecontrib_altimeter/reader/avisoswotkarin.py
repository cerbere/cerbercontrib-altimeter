import re
from pathlib import Path
from typing import Union, Optional

import xarray as xr

from cerbere.reader.basereader import BaseReader


class AVISOSWOTKarinBase(BaseReader):
    pattern: str = re.compile(r"^.*SWOT_L2_LR_SSH.*.nc$")
    engine: str = "netcdf4"
    description: str = "Use AVISO SWOT Karin L2 products in Xarray"
    url: str = "https://link_to/your_backend/documentation"

    @classmethod
    def open_dataset(
            cls,
            filename_or_obj: Union[str, Path],
            side: Optional[str] = None,
            **kwargs) -> xr.Dataset:
        """"""
        dss = None
        if side is not None:
            dss = xr.open_dataset(
                filename_or_obj=filename_or_obj, group=side, **kwargs)

        # to get attributes
        ds = xr.open_dataset(filename_or_obj=filename_or_obj)

        if dss is None:
            dss = ds
        else:
            dss.attrs = ds.attrs
            ds.close()

        #merged_ds = merged_ds.drop_vars(['time_tai'])

        # change lon to [-180,180]
        lons = dss['longitude'].data
        lons[lons>180] -= 360.
        dss['longitude'][:] = lons
        for att in ['geospatial_lon_max', 'geospatial_lon_min']:
            if dss.attrs[att] > 180:
                dss.attrs[att] -= 360.

        dss = dss.swap_dims({'num_lines': 'row', 'num_pixels': 'cell'})

        dss.time.attrs['axis'] = 'T'

        return dss


class AVISOSWOTKarinL(AVISOSWOTKarinBase):
    """Open only left side"""
    @classmethod
    def open_dataset(
            cls,
            filename_or_obj: Union[str, Path],
            side: Optional[str] = None,
            **kwargs) -> xr.Dataset:
        """"""
        return super().open_dataset(filename_or_obj, side='left', **kwargs)


class AVISOSWOTKarinR(AVISOSWOTKarinBase):
    """Open only right side"""
    @classmethod
    def open_dataset(
            cls,
            filename_or_obj: Union[str, Path],
            side: Optional[str] = None,
            **kwargs) -> xr.Dataset:
        """"""
        return super().open_dataset(filename_or_obj, side='right', **kwargs)


class AVISOSWOTKarin(AVISOSWOTKarinBase):
    """Open dataset merging left and right sides"""
    group_name: str = '/left'

    @classmethod
    def open_dataset(
            cls,
            filename_or_obj: Union[str, Path],
            side: Optional[str] = None,
            **kwargs) -> xr.Dataset:
        """"""
        dsl = super().open_dataset(filename_or_obj, side='left', **kwargs)
        dsr = super().open_dataset(filename_or_obj, side='right', **kwargs)

        ds = xr.concat([dsl, dsr], dim='cell')

        return ds
