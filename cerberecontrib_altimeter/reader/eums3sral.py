import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader


class EUMS3SRAL(BaseReader):
    pattern: str = re.compile(r"^.*S3B_SR_2_WAT____[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}.*SEN3$")
    engine: str = "netcdf4"
    description: str = "Use EUMETSAT Sentinel3 STD and RED products in Xarray"
    url: str = "https://link_to/your_backend/documentation"
    dim_name: str = 'time_01'
    product_name: str = 'standard'

    @staticmethod
    def set_index_and_coords(ds: xr.Dataset):
        ds = (
            ds.set_index({'time': 'time'})
              .reset_coords()
              .set_coords(['lon', 'lat', 'time'])
        )

        return ds

    @staticmethod
    def readjust_lon(ds: xr.Dataset):
        # longitude convention
        attrs = ds['lon'].attrs
        encoding = ds['lon'].encoding
        if 'lon' in ds.indexes:
            ds = ds.reset_index('lon')
        lon = ds.lon.values
        lon[lon > 180] -= 360
        ds['lon'] = xr.DataArray(data=lon, dims='time')
        ds['lon'].attrs = attrs
        ds['lon'].encoding = encoding

        return ds

    @classmethod
    def open_dataset(cls, filename_or_obj: Union[str, Path], **kwargs):
        files = list(Path(filename_or_obj).glob('*{0}*.nc'.format(cls.product_name)))
        if len(files) != 1:
            raise IOError(f'file of type {cls.product_name} not found in {filename_or_obj}')
        target_filename = Path(files[0])

        ds = xr.open_dataset(filename_or_obj=target_filename, **kwargs)

        dropped_vars = []
        for v in ds.variables:
            if cls.dim_name not in ds.variables[v].dims:
                dropped_vars.append(v)

        ds = ds.drop_vars(dropped_vars)

        return ds


class EUMS3SRALSTD1Hz(EUMS3SRAL):
    dim_name: str = 'time_01'
    product_name: str = 'standard'

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        ds = ds.rename_vars(
            {
                'time_01': 'time',
                'lat_01': 'lat',
                'lon_01': 'lon',
            }
        )
        ds = ds.swap_dims(
            {
                'time_01': 'time',
            }
        )

        ds = cls.set_index_and_coords(ds)
        return cls.readjust_lon(ds)


class EUMS3SRALSTD20HzC(EUMS3SRAL):
    dim_name: str = 'time_20_c'
    product_name: str = 'standard'

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        ds = ds.rename_vars(
            {
                'time_20_c': 'time',
                'lat_20_c': 'lat',
                'lon_20_c': 'lon',
            }
        )

        ds = ds.swap_dims({'time_20_c': 'time'})
        ds = cls.set_index_and_coords(ds)
        return cls.readjust_lon(ds)


class EUMS3SRALSTD20HzKu(EUMS3SRAL):
    dim_name: str = 'time_20_ku'
    product_name: str = 'standard'

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        ds = ds.rename_vars(
            {
                'time_20_ku': 'time',
                'lat_20_ku': 'lat',
                'lon_20_ku': 'lon',
            }
        )

        ds = ds.swap_dims({'time_20_ku': 'time'})
        ds = cls.set_index_and_coords(ds)
        return cls.readjust_lon(ds)


class EUMS3SRALRED1Hz(EUMS3SRAL):
    dim_name: str = 'time_01'
    product_name: str = 'reduced'

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        ds = ds.rename_vars(
            {
                'time_01': 'time',
                'lat_01': 'lat',
                'lon_01': 'lon',
            }
        )
        ds = ds.swap_dims(
            {
                'time_01': 'time',
            }
        )

        ds = cls.set_index_and_coords(ds)
        return cls.readjust_lon(ds)
