import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader


class AVISOSARAL(BaseReader):
    # ex: SRL_GPS_2PfP019_0523_20141222_111417_20141222_120436.CNES.nc
    pattern: str = re.compile(r"^.*SRL_GPS_.*_[0-9]{8}_[0-9]{6}_[0-9]{8}.*.nc$")
    engine: str = "netcdf4"
    description: str = ("For reading AVISO SARAL Version F IGDR, GDR and "
                        "SGDR products with cerbere")
    url: str = "https://gitlab.ifremer.fr/cerbere/cerbercontrib-altimeter/"


class AVISOSARAL1Hz(AVISOSARAL):

    @classmethod
    def postprocess(cls, dst: xr.Dataset, **kwargs):
        # remove 40 Hz related variables
        dst = dst.drop_dims("meas_ind")

        # longitudes between -180 and 180
        dst['lon'][dst['lon'] > 180] -= 360

        return dst


class AVISOSARAL20Hz(AVISOSARAL):

    @classmethod
    def postprocess(cls, dst: xr.Dataset, **kwargs):

        # keep only variables with meas_ind dimension
        vars_40hz = []
        for v in dst.variables:
            dims_40hz = set(['time', 'meas_ind'])
            if len(set(dst[v].dims).intersection(dims_40hz)) in [0, 2]:
                vars_40hz.append(v)
        dst = dst[vars_40hz]

        # flatten
        dst = dst.stack(z=('time', 'meas_ind')).reset_index('z').drop_vars(
            ['time', 'lat', 'lon']).rename(
            {'time_40hz': 'time', 'lat_40hz': 'lat', 'lon_40hz': 'lon'}
        ).swap_dims({'z': 'time'}).reset_coords(['meas_ind'])

        # longitudes between -180 and 180
        dst['lon'][dst['lon'] > 180] -= 360

        return dst
