import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader
from dateutil import parser


class EUMETSATSentinel6(BaseReader):
    pattern: str = re.compile(r"^.*S6A_P4_2__[H,L]R______[0-9]{8}T[0-9]{6}_["
                              r"0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}.*SEN6$")
    engine: str = "netcdf4"
    description: str = "Use EUMETSAT Sentinel6 STD and RED products in Xarray"
    url: str = "https://gitlab.ifremer.fr/cerbere/cerbercontrib-altimeter"
    group_name: str = '/data_01'
    product_name: str = '_STD_'

    @classmethod
    def open_dataset(cls, filename_or_obj: Union[str, Path], **kwargs):

        files = list(Path(filename_or_obj).glob(
            '*{0}*.nc'.format(cls.product_name)))
        if len(files) != 1:
            raise IOError(
                f'file of type {cls.product_name} not found in '
                f'{filename_or_obj}')

        target_filename = Path(files[0])

        dst = xr.open_dataset(
            filename_or_obj=target_filename, group=cls.group_name, **kwargs)

        try:
            ds_c = xr.open_dataset(
                filename_or_obj=target_filename,
                group='{0}{1}'.format(cls.group_name, '/c'),
                **kwargs,
            )
            ds_c = ds_c.rename_vars(
                {var: '{0}{1}'.format('c_', var) for var in ds_c.variables})
            if 'c_time' in ds_c:
                ds_c = ds_c.reset_index(['c_time'])
        except OSError:
            # HR dataset does not contain C band
            ds_c = None

        ds_ku = xr.open_dataset(
            filename_or_obj=target_filename,
            group='{0}{1}'.format(cls.group_name, '/ku'),
            **kwargs,
        )
        ds_ku = ds_ku.rename_vars(
            {var: '{0}{1}'.format('ku_', var) for var in ds_ku.variables})
        if 'ku_time' in ds_ku:
            ds_ku = ds_ku.reset_index(['ku_time'])

        ds = xr.open_dataset(filename_or_obj=target_filename)

        if ds_c is not None:
            merged_ds = xr.merge([dst, ds_c, ds_ku])
        else:
            merged_ds = xr.merge([dst, ds_ku])

        # default coordinates
        if 'c_longitude' in merged_ds:
            merged_ds['lon'] = merged_ds['c_longitude']
            merged_ds['lat'] = merged_ds['c_latitude']
            merged_ds['time'] = merged_ds['c_time']
        else:
            merged_ds = merged_ds.rename_vars(
                {'longitude': 'lon', 'latitude': 'lat'})
        merged_ds = (merged_ds.set_index({'time': 'time'})
                     .reset_coords()
                     .set_coords(['lon', 'lat', 'time']))

        merged_ds.attrs.update(ds.attrs)
        ds.close()

        merged_ds.attrs['time_coverage_start'] = merged_ds.attrs[
            'first_measurement_time']
        merged_ds.attrs['time_coverage_end'] = merged_ds.attrs[
            'last_measurement_time']

        # change lon to [-180,180]
        if 'c_longitude' in merged_ds:
            merged_ds['c_longitude'][merged_ds.c_longitude > 180] -= 360
            merged_ds['ku_longitude'][merged_ds.ku_longitude > 180] -= 360
        merged_ds['lon'][merged_ds.lon > 180] -= 360

        # drop_vars = ['time_tai', 'c_time_tai', 'ku_time_tai']
        # merged_ds = merged_ds.drop_vars(
        #     [var for var in merged_ds.variables if var in drop_vars]
        # )

        return merged_ds


class EUMETSATSentinel6STD1Hz(EUMETSATSentinel6):
    group_name: str = '/data_01'
    product_name: str = '_STD_'


class EUMETSATSentinel6STD20Hz(EUMETSATSentinel6):
    group_name: str = '/data_20'
    product_name: str = '_STD_'


class EUMETSATSentinel6RED1Hz(EUMETSATSentinel6):
    group_name: str = '/data_01'
    product_name: str = '_RED_'
