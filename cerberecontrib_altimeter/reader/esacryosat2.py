import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader
from dateutil import parser

DEFAULT_TIME_UNITS = "seconds since 2000-01-01 00:00:00.0"


class ESACryosat2(BaseReader):
    pattern: str = re.compile(
        r"^.*CS_OFFL_SIR_LRM_[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}.*PAC_R_NT_003.nc$",
    )
    engine: str = "netcdf4"
    description: str = "Use Cryosat-2 Altimeter products from ESA in Xarray"
    url: str = "https://link_to/your_backend/documentation"
    time_units: str = DEFAULT_TIME_UNITS


class ESACryosat21Hz(ESACryosat2):

    @classmethod
    def postprocess(cls, ds: xr.Dataset, decode_times: bool = True):
        drop_vars = []
        for k, v in ds.variables.mapping.items():
            if 'time_20_ku' in v.dims:
                drop_vars.append(k)
        ds = ds.drop_vars(drop_vars)

        if 'time_avg_01_ku' in ds:
            ds = ds.rename({
                'time_avg_01_ku': 'time',
                'lat_avg_01_ku': 'lat',
                'lon_avg_01_ku': 'lon'})
        else:
            ds = ds.rename({
                'time_cor_01': 'time', 'lat_01': 'lat', 'lon_01': 'lon'})

        # remove fossiles of dropped dimensions (xarray bug in rename?)
        ds.encoding['unlimited_dims'] = (
                ds.encoding['unlimited_dims'] - {'time_20_ku'})
        if 'time_avg_01_ku' in ds.encoding['unlimited_dims']:
                ds.encoding['unlimited_dims'].discard('time_avg_01_ku')
        elif 'time_cor_01' in ds.encoding['unlimited_dims']:
                ds.encoding['unlimited_dims'].discard('time_cor_01')
        ds.encoding['unlimited_dims'].add('time')

        ds['lon'].attrs['axis'] = 'X'
        ds['lat'].attrs['axis'] = 'Y'
        ds['time'].attrs['axis'] = 'T'

        ds.attrs['time_coverage_start'] = parser.parse(
            ds.attrs['first_record_time'].replace('TAI=', ''))
        ds.attrs['time_coverage_end'] = parser.parse(
            ds.attrs['last_record_time'].replace('TAI=', ''))

        return ds


class ESACryosat220Hz(ESACryosat2):

    @classmethod
    def postprocess(cls, ds: xr.Dataset, **kwargs):
        drop_vars = []
        for k, v in ds.variables.mapping.items():
            if 'time_avg_01_ku' in v.dims or 'time_cor_01' in v.dims:
                drop_vars.append(k)
        ds = ds.drop_vars(drop_vars)

        if 'lat_poca_20_ku' in ds:
            ds = ds.rename(
                {'time_20_ku': 'time', 'lat_poca_20_ku': 'lat',
                 'lon_poca_20_ku': 'lon'})
        else:
            ds = ds.rename(
                {'time_20_ku': 'time', 'lat_20_ku': 'lat', 'lon_20_ku': 'lon'}
            )

        # remove fossiles of dropped dimensions (xarray bug in rename?)
        ds.encoding['unlimited_dims'] = (
            ds.encoding['unlimited_dims'] - {'time_cor_01', 'time_avg_01_ku'})
        if 'time_20_ku' in ds.encoding['unlimited_dims']:
            ds.encoding['unlimited_dims'].discard('time_20_ku')
            ds.encoding['unlimited_dims'].add('time')

        ds['lon'].attrs['axis'] = 'X'
        ds['lat'].attrs['axis'] = 'Y'
        ds['time'].attrs['axis'] = 'T'

        ds.attrs['time_coverage_start'] = parser.parse(
            ds.attrs['first_record_time'].replace('TAI=', ''))
        ds.attrs['time_coverage_end'] = parser.parse(
            ds.attrs['last_record_time'].replace('TAI=', ''))

        return ds
