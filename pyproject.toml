[build-system]
requires = ["poetry-core>=1.0.0", "poetry-dynamic-versioning"]
build-backend = "poetry_dynamic_versioning.backend"

[tool.poetry]
name = "cerberecontrib-altimeter"
version = "3.0.0"
description = "Cerbere extension for various altimeter products"
authors = ["PIOLLE <jean.francois.piolle@ifremer.fr>"]
license = "GPL-3.0-or-later"
readme = "README.rst"
classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
]
keywords = ["cerbere", "contrib", "altimeter"]

exclude = [
    # exclude tests directory from wheel (only on sdist)
    { path = "tests", format = "wheel" }
]
[[tool.poetry.source]]
name = "release"
url = "https://gitlab.ifremer.fr/api/v4/projects/1225/packages/pypi/simple/"
default = false
secondary = true

[tool.poetry-dynamic-versioning]
enable = false
vcs = "git"
style = "semver"
pattern = "^v?(?P<base>\\d+\\.\\d+\\.\\d+)(-?((?P<stage>[a-zA-Z]+)\\.?(?P<revision>\\d+)?))?$"

[tool.poetry.dependencies]
python = ">=3.9"
cerbere = "^3.0.0"

[tool.poetry.dev-dependencies]
# execute commands before commit
pre-commit = "^2.15.0"
# static analysis
wemake-python-styleguide = "^0.16.0"
flake8 = "*"
# unit tests
pytest = "^6"
pytest-cov = "^2"
coverage = { version="*", extras=["toml"]}
# commit + version
commitizen = "^2.21.2"

[tool.poetry.plugins."cerbere.reader"]
"AVISOJason3F1Hz" = "cerberecontrib_altimeter.reader.avisojason3f:AVISOJason3F1Hz"
"AVISOJason3F20Hz" = "cerberecontrib_altimeter.reader.avisojason3f:AVISOJason3F20Hz"
"AVISOJason1Hz" = "cerberecontrib_altimeter.reader.avisojason:AVISOJason1Hz"
"AVISOJason20Hz" = "cerberecontrib_altimeter.reader.avisojason:AVISOJason20Hz"
"AVISOSARAL1Hz" = "cerberecontrib_altimeter.reader.avisosaral:AVISOSARAL1Hz"
"AVISOSARAL20Hz" = "cerberecontrib_altimeter.reader.avisosaral:AVISOSARAL20Hz"

"EUMETSATSentinel6STD1Hz" = "cerberecontrib_altimeter.reader.eumetsatsentinel6:EUMETSATSentinel6STD1Hz"
"EUMETSATSentinel6STD20Hz" = "cerberecontrib_altimeter.reader.eumetsatsentinel6:EUMETSATSentinel6STD20Hz"
"EUMETSATSentinel6RED1Hz" = "cerberecontrib_altimeter.reader.eumetsatsentinel6:EUMETSATSentinel6RED1Hz"

"EUMS3SRALSTD1Hz" = "cerberecontrib_altimeter.reader.eums3sral:EUMS3SRALSTD1Hz"
"EUMS3SRALSTD20HzC" = "cerberecontrib_altimeter.reader.eums3sral:EUMS3SRALSTD20HzC"
"EUMS3SRALSTD20HzKu" = "cerberecontrib_altimeter.reader.eums3sral:EUMS3SRALSTD20HzKu"
"EUMS3SRALRED1Hz" = "cerberecontrib_altimeter.reader.eums3sral:EUMS3SRALRED1Hz"

"Sentinel3LRRMC" = "cerberecontrib_altimeter.reader.sentinel3lrrmc:Sentinel3LRRMC"

"ESAEnvisatRA21Hz" = "cerberecontrib_altimeter.reader.esaenvisatra2:ESAEnvisatRA21Hz"
"ESAEnvisatRA220Hz" = "cerberecontrib_altimeter.reader.esaenvisatra2:ESAEnvisatRA220Hz"

"ESACryosat21Hz" = "cerberecontrib_altimeter.reader.esacryosat2:ESACryosat21Hz"
"ESACryosat220Hz" = "cerberecontrib_altimeter.reader.esacryosat2:ESACryosat220Hz"

"AVISOSWOTKarinL" = "cerberecontrib_altimeter.reader.avisoswotkarin:AVISOSWOTKarinL"
"AVISOSWOTKarinR" = "cerberecontrib_altimeter.reader.avisoswotkarin:AVISOSWOTKarinR"
"AVISOSWOTKarin" = "cerberecontrib_altimeter.reader.avisoswotkarin:AVISOSWOTKarin"


[tool.black]
line-length = 120

[tool.pycln]
all = true

[tool.isort]
line_length = 120
multi_line_output = 3
include_trailing_comma = true
force_grid_wrap = 0
use_parentheses = true
ensure_newline_before_comments = true
