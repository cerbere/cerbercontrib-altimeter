from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'ESACryosat2',
    'CS_OFFL_SIR_LRM_2__20240409T001429_20240409T004207_E001.nc'
)


@pytest.fixture(scope='module')
def reader():
    return 'ESACryosat220Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
