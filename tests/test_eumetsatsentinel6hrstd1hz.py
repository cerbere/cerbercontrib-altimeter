from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'EUMETSATSentinel6',
    'S6A_P4_2__HR______20220101T003819_20220101T013432_20230309T122854_3373_042_073_036_EUM__REP_NT_F08.SEN6',
)


@pytest.fixture(scope='module')
def reader():
    return 'EUMETSATSentinel6STD1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
