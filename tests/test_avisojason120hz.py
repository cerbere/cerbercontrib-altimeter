from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'AVISOJason',
    'JA1_GPS_2PeP536_056_20130606_075650_20130606_085254.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOJason20Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
