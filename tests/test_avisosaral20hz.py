from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'AVISOSARAL',
    'SRL_GPS_2PfP019_0523_20141222_111417_20141222_120436.CNES.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOSARAL20Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
