from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'EUMS3SRAL',
    'S3B_SR_2_WAT____20220803T224724_20220803T233142_20220829T171927_2658_069_044______MAR_O_NT_005.SEN3',
)


@pytest.fixture(scope='module')
def reader():
    return 'EUMS3SRALSTD20HzKu'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
