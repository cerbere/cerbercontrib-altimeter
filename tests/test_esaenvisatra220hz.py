from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'ENVISATv3',
    'ENV_RA_2_MWS____20100101T000047_20100101T005104_20170905T025157_3017_085_0691____PAC_R_NT_003.nc'
)


@pytest.fixture(scope='module')
def reader():
    return 'ESAEnvisatRA220Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
