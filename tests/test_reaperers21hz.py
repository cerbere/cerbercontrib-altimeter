from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'AVISOJason',
    'E2_REAP_ERS_ALT_2S_20020828T083741_20020828T101841_RP01.NC',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOJason1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
