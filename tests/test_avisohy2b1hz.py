from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
  #  'AVISOJason',
    'H2B_OPER_SDR_2PC_0065_0101_20210416T110946_20210416T120201.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOJason1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
