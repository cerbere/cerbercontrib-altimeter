from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'AVISOJason',
    'TP_GPN_2PfP285_165_20000615_141822_20000615_151434.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOJason1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
