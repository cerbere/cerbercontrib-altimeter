from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'EUMETSATSentinel6',
    'S6A_P4_2__LR______20220629T002232_20220629T011845_20230322T160136_3373_060_086_043_EUM__REP_NT_F08.SEN6',
)


@pytest.fixture(scope='module')
def reader():
    return 'EUMETSATSentinel6RED1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
