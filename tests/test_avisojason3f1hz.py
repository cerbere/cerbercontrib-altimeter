from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'AVISOJason3F',
    'JA3_GPS_2PfP124_215_20190629_095729_20190629_105342.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOJason3F1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
