from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'Sentinel3LRRMC',
    'S3A_LRRMC_SGDR_C0007_P0004_20160726_000336_20160726_005405__S3PP_V2-1_PEACHI_PRODUCT.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'Sentinel3LRRMC'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
