from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'AVISOJason',
    'H2A_RA1_SDR_2PT_0087_0136_20150121_233150_20150122_002402.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOJason1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
