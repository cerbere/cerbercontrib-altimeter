=========================
cerberecontrib-altimeter
=========================

Cerbere reader classes for various altimeter products, including:

* AVISOJason1Hz : AVISO Jason-1 & 2 Version D & E IGDR, GDR and SGDR
  products, 1 Hz only parameters. Also works with NSOAS HY-2A SDR and TOPEX
  GDR version F
* AVISOJason20Hz : AVISO Jason-1 & 2 Version D & E IGDR, GDR and SGDR
  products, 20 Hz only parameters. Also works with NSOAS HY-2A SDR and TOPEX
  GDR version F
* AVISOJason3F1Hz : AVISO Jason-3 Version F IGDR, GDR and SGDR
  products, 1 Hz only parameters
* AVISOJason3F20Hz : AVISO Jason-3 Version F IGDR, GDR and SGDR
  products, 20 Hz only parameters
* AVISOSARAL1Hz : AVISO SARAL/AltiKa Version F IGDR, GDR and SGDR
  products, 1 Hz only parameters
* AVISOSARAL20Hz : AVISO SARAL/AltiKa Version F IGDR, GDR and SGDR
  products, 20 Hz only parameters
* Sentinel3LRRMC : CNES/CLS Sentinel-3A products retracked with
  LR-RMC retracker
* ESAEnvisatRA21Hz : ENVISAT version 3 SGDR products from ESA, 1 Hz only
  parameters
* ESAEnvisatRA220Hz : ENVISAT version 3 SGDR products from ESA, 20 Hz only
  parameters
* EUMETSATSentinel6STD1Hz : EUMETSAT Sentinel-6 low and high rate (LR/HR)
  standard products, 1 Hz only parameters
* EUMETSATSentinel6STD20Hz : EUMETSAT Sentinel-6 low and high rate (LR/HR)
  standard products, 20 Hz only parameters
* EUMETSATSentinel6RED1Hz : EUMETSAT Sentinel-6 low and high rate (LR/HR)
  reduced products, 1 Hz only parameters
* ESACryosat21Hz : ESA Cryosat-2 L1B and L2, version D and E, 1 Hz only 
  parameters
* ESACryosat220Hz : ESA Cryosat-2 L1B and L2, version D and E, 20 Hz only 
  parameters


.. code-block:: python

  # example : opening a AVISO Jason-3 product file for 1 Hz measurements
  import cerbere

  dst = cerbere.open_dataset(
      'JA3_IPN_2PfP181_175_20210113_010448_20210113_020100.nc',
      reader='AVISOJason3F1Hz',
  )

